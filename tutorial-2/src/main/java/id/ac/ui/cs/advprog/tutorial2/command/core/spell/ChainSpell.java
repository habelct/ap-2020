package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> stack;
    public ChainSpell(ArrayList<Spell> stack){
        this.stack = stack;
    }



    @Override
    public void undo() {
        for (int i = stack.size()-1; i >= 0; i--){
            stack.get(i).undo();
        }

    }

    @Override
    public void cast() {
        for(int j = 0; j < stack.size(); j++){
            stack.get(j).cast();
        }
    }


    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
