package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    protected String childName;
    protected String childRole;

    //TODO: Complete me
    public OrdinaryMember(String childName, String childRole){
        this.childName = childName;
        this.childRole = childRole;
    }

    public String getName(){
        return this.childName;
    }

    public String getRole(){
        return this.childRole;
    }

    public void addChildMember(Member member){

    }
    public void removeChildMember(Member member){

    }
    public List<Member> getChildMembers(){
        return new ArrayList<>();
    }


}
