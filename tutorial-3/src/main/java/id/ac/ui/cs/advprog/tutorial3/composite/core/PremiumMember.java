package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
        //TODO: Complete me
    protected String childName;
    protected String childRole;
    protected List<Member> childMembers = new ArrayList<Member>();

    public PremiumMember(String childName, String childRole){
        this.childName = childName;
        this.childRole = childRole;
    }
    public String getName(){
        return this.childName;
    }
    public String getRole(){
        return this.childRole;
    }
    public void addChildMember(Member member){
        if(this.getRole().equals("Master")){
            this.childMembers.add(member);
        }

        else{
            if(childMembers.size() < 3){
                this.childMembers.add(member);
            }




        }

    }
    public void removeChildMember(Member member){
        this.childMembers.remove(member);

    }

    public List<Member> getChildMembers(){
        return this.childMembers;
    }
}
