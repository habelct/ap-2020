package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int min = 5;
        int max = 11;
        double sum = Math.random() * (max - min + 1) + min;
        Random ran = new Random();
        int nxt = ran.nextInt(6);
        int hasil = min + nxt;
        if(weapon == null) return hasil;
        return weapon.getWeaponValue() + hasil;

    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Raw " + this.weapon.getDescription();
    }
}
