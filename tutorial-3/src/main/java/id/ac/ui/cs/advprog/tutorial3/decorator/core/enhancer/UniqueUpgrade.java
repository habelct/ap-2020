package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class UniqueUpgrade extends Weapon {

    Weapon weapon;

    public UniqueUpgrade(Weapon weapon){

        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }


    // Senjata bisa dienhance hingga 10-15 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        int min = 10;
        int max = 16;
        Random ran = new Random();
        int nxt = ran.nextInt(6);
//        double sum = Math.random() * (max - min + 1) + min;
        int hasil = min + nxt;
        if(weapon == null) return hasil;
        return weapon.getWeaponValue() + hasil;

    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "Unique " + this.weapon.getDescription();
    }
}
