package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member x = new OrdinaryMember("Anak", "Test");
        member.addChildMember(x);
        assertEquals(1,member.getChildMembers().size());

    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member x = new OrdinaryMember("Anak", "Test");
        member.addChildMember(x);
        member.removeChildMember(x);
        assertEquals(0,member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member x = new OrdinaryMember("x", "Test");
        Member y = new OrdinaryMember("y", "Test");
        Member z = new OrdinaryMember("z", "Test");
        Member v = new OrdinaryMember("v", "Test");
        member.addChildMember(x);
        member.addChildMember(y);
        member.addChildMember(z);
        member.addChildMember(v);
        assertEquals(3,member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member x = new PremiumMember("x", "Master");
        Member y = new OrdinaryMember("y", "Test");
        Member z = new OrdinaryMember("z", "Test");
        Member v = new OrdinaryMember("v", "Test");
        Member a = new OrdinaryMember("a", "Test");
        x.addChildMember(y);
        x.addChildMember(z);
        x.addChildMember(v);
        x.addChildMember(a);

        assertEquals(4, x.getChildMembers().size());


    }
}
