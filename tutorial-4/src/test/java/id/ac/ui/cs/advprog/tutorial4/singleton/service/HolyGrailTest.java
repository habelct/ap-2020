package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests
    @Mock
    private HolyGrail holyGrail;

    @Test
    public void testGetHolyWish() {
        Mockito.when(holyGrail.getHolyWish()).thenReturn(HolyWish.getInstance());
        assertTrue((holyGrail.getHolyWish() instanceof HolyWish));
    }
}


